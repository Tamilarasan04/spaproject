import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:spa_app/const/config.dart';
import 'package:spa_app/models/slider.dart';
import 'dart:convert';
import 'package:spa_app/models/spa_models.dart';

class SpaService {
  Future<SliderModel> getService() async {
    Response response = await http.get(Uri.parse(SERVICE_URL), headers: {
      "Authorization": "Bearer " + TOKEN,
      "Accept": "application/json"
    });
    if (response.statusCode == 200) {
      SliderModel slidermodel = SliderModel.fromJson(jsonDecode(response.body));
      return slidermodel;
    } else {
      return SliderModel();
    }
  }

  Future<SpaModel> postService(Services service) async {
    Response response = await http.post(
        Uri.parse("https://admin.blissme.hk/public/api/servicelist"),
        headers: {
          "Authorization": "Bearer " + TOKEN,
          "Accept": "application/json"
        },
        body: {
          "serviceId": service.serviceId.toString(),
          "page": "2",
          "limit": "5"
        });
    if (response.statusCode == 200) {
      SpaModel spamodel = SpaModel.fromJson(jsonDecode(response.body));
      print("Success $spamodel");

      return spamodel;
    } else {
      print("Error ${response.statusCode}");

      return SpaModel();
    }
  }
}
