import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:spa_app/models/slider.dart';
import 'package:spa_app/services/spa_services.dart';
import 'package:spa_app/models/spa_models.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:auto_size_text/auto_size_text.dart';

class CategoryList extends StatefulWidget {
  final Services service;
  CategoryList({Key key, this.service}) : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  // @override
  // void initState() {
  //   super.initState();
  //   SpaService().postService(widget.service);
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<SpaModel>(
          future: SpaService().postService(widget.service),
          builder: (context, snapdata) {
            if (snapdata.hasData) {
              List<Spa> spalist = snapdata.data.spa;
              if (spalist.length > 0) {
                return ListView(
                  children: spalist
                      .map((e) => Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10.0,
                          ),
                          child: Card(
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Row(
                              children: [
                                SizedBox(
                                    width: 100.w,
                                    height: 102.h,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(5),
                                      child: Image.network(e.spaImage,
                                          fit: BoxFit.fill),
                                    )),
                                SizedBox(width: 10.w),
                                Container(
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(e.spaName,
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight:
                                                        FontWeight.w600))),
                                        SizedBox(height: 2.h),
                                        Text(e.message,
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                              fontSize: 10.sp,
                                            ))),
                                        SizedBox(height: 2.h),
                                        Text(
                                            e.openTime +
                                                e.openMeridiem +
                                                "-" +
                                                e.closeTime +
                                                e.closeMeridiem,
                                            style: GoogleFonts.poppins(
                                                color: Colors.green,
                                                textStyle: TextStyle(
                                                    fontSize: 12.sp,
                                                    fontWeight:
                                                        FontWeight.w600))),
                                        SizedBox(height: 2.h),
                                        Row(children: [
                                          Container(
                                              child: Row(
                                            children: [
                                              Icon(Icons.location_pin,
                                                  size: 18.sp),
                                              Text(e.distance + e.distanceUnit,
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                    fontSize: 12.sp,
                                                  )))
                                            ],
                                          )),
                                          Container(
                                            alignment: Alignment.bottomRight,
                                            width: 180.h,
                                            height: 30.h,
                                            child: ElevatedButton(
                                              onPressed: () {},
                                              style: ElevatedButton.styleFrom(
                                                primary: Colors.pinkAccent,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                ),
                                                elevation: 2.0,
                                              ),
                                              child: FittedBox(
                                                child: Text(
                                                  'Book',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ])
                                      ],
                                    ))
                              ],
                            ),
                          )))
                      .toList(),
                );
              } else {
                return Center(child: Text("No Data Found"));
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}
