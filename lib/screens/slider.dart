import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:spa_app/models/slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SpaSlider extends StatefulWidget {
  final List<Carousel> carousellist;
  SpaSlider({this.carousellist});
  @override
  _SpaSliderState createState() => _SpaSliderState();
}

class _SpaSliderState extends State<SpaSlider> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: 180.h,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        scrollDirection: Axis.horizontal,
      ),
      items: widget.carousellist
          .map((item) => Builder(builder: (BuildContext context) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 2.0),
                  child: Image.network(
                    item.image,
                    fit: BoxFit.fill,
                  ),
                );
              }))
          .toList(),
    );
  }
}
