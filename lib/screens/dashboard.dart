import 'package:flutter/material.dart';
import 'package:spa_app/const/styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spa_app/models/slider.dart';
import 'package:spa_app/services/spa_services.dart';
import 'package:spa_app/screens/slider.dart';
import 'package:spa_app/screens/tab_view.dart';

class DashBoard extends StatefulWidget {
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appbgColor,
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        leading: Icon(Icons.menu, color: iconColor),
        title: Text("Top Categories",
            style: TextStyle(
              color: Colors.black,
              fontSize: 18.sp,
            )),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(
              Icons.search,
              color: iconColor,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: FutureBuilder<SliderModel>(
            future: SpaService().getService(),
            builder: (context, snapshotdata) {
              if (snapshotdata.hasData) {
                SliderModel slidermodel = snapshotdata.data;
                List<Carousel> carousellist = slidermodel.carousel;
                List<Services> services = slidermodel.services;
                return Container(
                    child: Column(
                  children: [
                    SpaSlider(
                      carousellist: carousellist,
                    ),
                    SizedBox(height: 15.h),
                    Container(
                        height: 500.h, child: TabView(services: services)),
                  ],
                ));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.store,
            ),
            label: "Nearby",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_bag,
            ),
            label: "Product",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_cart,
            ),
            label: "Cart",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: "Profile",
          ),
        ],
      ),
    );
  }
}
