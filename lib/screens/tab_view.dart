import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:spa_app/models/slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spa_app/screens/spa_list.dart';

class TabView extends StatefulWidget {
  TabView({Key key, this.services}) : super(key: key);

  final List<Services> services;

  @override
  _TabViewState createState() => _TabViewState();
}

class _TabViewState extends State<TabView> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.services.length,
      child: Column(
        children: <Widget>[
          ButtonsTabBar(
              height: 45.h,
              radius: 100.0,
              borderColor: Colors.grey,
              borderWidth: 0.5,
              backgroundColor: Colors.pink,
              unselectedBackgroundColor: Colors.white,
              unselectedLabelStyle: TextStyle(
                color: Colors.grey,
                fontSize: 13.sp,
              ),
              labelStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 13.sp,
                  fontWeight: FontWeight.bold),
              tabs: widget.services
                  .map((e) => Tab(
                        text: e.serviceName,
                      ))
                  .toList()),
          Expanded(
              child: TabBarView(
                  children: widget.services
                      .map((t) => CategoryList(service: t))
                      .toList())),
          SizedBox(height: 10.h),
        ],
      ),
    );
  }
}
