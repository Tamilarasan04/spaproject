class SliderModel {
  bool status;
  List<Carousel> carousel;
  List<Services> services;

  SliderModel({this.status, this.carousel, this.services});

  SliderModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['carousel'] != null) {
      carousel = new List<Carousel>();
      json['carousel'].forEach((v) {
        carousel.add(new Carousel.fromJson(v));
      });
    }
    if (json['services'] != null) {
      services = new List<Services>();
      json['services'].forEach((v) {
        services.add(new Services.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.carousel != null) {
      data['carousel'] = this.carousel.map((v) => v.toJson()).toList();
    }
    if (this.services != null) {
      data['services'] = this.services.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Carousel {
  String image;
  String type;
  int id;

  Carousel({this.image, this.type, this.id});

  Carousel.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    type = json['type'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['type'] = this.type;
    data['id'] = this.id;
    return data;
  }
}

class Services {
  int serviceId;
  String serviceName;
  int spaCount;

  Services({this.serviceId, this.serviceName, this.spaCount});

  Services.fromJson(Map<String, dynamic> json) {
    serviceId = json['serviceId'];
    serviceName = json['serviceName'];
    spaCount = json['spaCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['serviceId'] = this.serviceId;
    data['serviceName'] = this.serviceName;
    data['spaCount'] = this.spaCount;
    return data;
  }
}
